/** @type {import('next').NextConfig} */
const nextConfig = {
    env:{
        AW_PROJ_ID: process.env.AW_PROJ_ID
    }
}

require("dotenv").config

module.exports = nextConfig