import Head from 'next/head'

export default function TalentPage() {
    return (
        <main>
            <Head>
                <title>Talent List - TuuperCove</title>
            </Head>
        </main>
    )
}